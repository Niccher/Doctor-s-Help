/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Highway;

import java.sql.Connection;
import com.mysql.jdbc.Driver;
import java.sql.*;
import java.util.Locale;
import javax.swing.JOptionPane;

/**
 *
 * @author nicch
 */
public class Dbs {
    private Connection conn=null;
    
        public static Connection InitDb() {
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/","root","");

                PreparedStatement dbs = conn.prepareStatement("CREATE DATABASE IF NOT EXISTS `HighwayMed`");
                dbs.execute();

               Statement stt = conn.createStatement();
               stt.execute("USE HighwayMed");
               
               String Std = "CREATE TABLE IF NOT EXISTS `tbl_Staff` (`Count` INT AUTO_INCREMENT UNIQUE, `Name` VARCHAR(35) NOT NULL , `NationalID` INT(8) NOT NULL , `StaffID` INT(6) NOT NULL PRIMARY KEY , `Username` VARCHAR(20) NOT NULL , `Password` VARCHAR(20) NOT NULL ,`Contacts` INT(10) NOT NULL ,`Level` VARCHAR(10) NOT NULL, `Avatar` LONGBLOB NOT NULL )";
               PreparedStatement pst1 = conn.prepareStatement(Std);
               pst1.execute();
               
               
               String Std3 = "CREATE TABLE IF NOT EXISTS `tbl_Clients` ( `Count` INT AUTO_INCREMENT UNIQUE,`Name` VARCHAR(35) NOT NULL , `NationalID` INT(8) NOT NULL, `ClientID` INT(6) NOT NULL PRIMARY KEY,`Region` VARCHAR(20) NOT NULL,`Date` VARCHAR(15) NOT NULL,`Contacts` INT(10) NOT NULL, `Avatar` LONGBLOB NOT NULL )";
               PreparedStatement pst3 = conn.prepareStatement(Std3);
               pst3.execute();
               
               String Std4 = "CREATE TABLE IF NOT EXISTS `tbl_Stock` ( `Count` INT AUTO_INCREMENT UNIQUE, `Name` VARCHAR(10) NOT NULL , `StockDate` VARCHAR(10) NOT NULL ,`StockTime` VARCHAR(10) NOT NULL ,`Company` VARCHAR(20) NOT NULL,`Unit` VARCHAR(20) NOT NULL,`StockID` INT(10) NOT NULL,`StockDesc` VARCHAR(40) NOT NULL ,`StockScope` VARCHAR(25) NOT NULL,`SideEffect` VARCHAR(25) NOT NULL)";
               PreparedStatement pst4 = conn.prepareStatement(Std4);
               pst4.execute();
               
               String Std5 = "CREATE TABLE IF NOT EXISTS `tbl_Logs` ( `Count` INT AUTO_INCREMENT UNIQUE, `Date` VARCHAR(10) NOT NULL ,`Time` VARCHAR(6) NOT NULL ,`Username` VARCHAR(20) NOT NULL ,`Password` VARCHAR(10)  NOT NULL ,`Level` VARCHAR(10) NOT NULL ,`Correct` VARCHAR(10) NOT NULL)";
               PreparedStatement pst5 = conn.prepareStatement(Std5);
               pst5.execute();
               
               String Std6 = "CREATE TABLE IF NOT EXISTS `tbl_Admine` ( `Count` INT AUTO_INCREMENT UNIQUE, `AcDate` VARCHAR(10) NOT NULL ,`AcTime` VARCHAR(10) NOT NULL ,`Task` VARCHAR(30) NOT NULL ,`Output` VARCHAR(10)  NOT NULL)";
               PreparedStatement pst6 = conn.prepareStatement(Std6);
               pst6.execute();
               
               String Std7 = "CREATE TABLE IF NOT EXISTS `tbl_Treats` (`Count` INT AUTO_INCREMENT UNIQUE ,`Name` VARCHAR(20) NOT NULL ,`NationalID`  VARCHAR(20) NOT NULL ,`ClientID` INT(10) NOT NULL ,`Date` VARCHAR(10) NOT NULL ,`Time` VARCHAR(10) NOT NULL ,`Conditn` VARCHAR(30) NOT NULL ,`Diagnosis` VARCHAR(30) NOT NULL ,`Lab` VARCHAR(30) NOT NULL ,`Drugs`  VARCHAR(30) NOT NULL )";
               PreparedStatement pst7 = conn.prepareStatement(Std7);
               pst7.execute();

                Statement stmt=conn.createStatement();
                return conn;

            }catch(Exception ex){
                JOptionPane.showMessageDialog(null,ex+"\nDatabase  Error 2");
                return  null;
            }
            //return null;
        }
        
        public static void main(String[] args) {
        new Dbs();
    }
    
}
